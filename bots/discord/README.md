Collinks bot for discord
========================

## Requirements  
Node.JS version 12

npm i -g nodemon

npm install discord.js

## How to run the bot for testing
Copy the sample auth.json.example file to auth.json and fill in the bot authentication token
Keep in mind that this token must not be pushed to git or published anywhere on the internet.

Use nodemon to run the bot :
`nodemon --inspect bot.js`

## How to add the bot to a discord "server"
Use this URL : https://discord.com/api/oauth2/authorize?client_id=708035987003146240&permissions=126032&scope=bot