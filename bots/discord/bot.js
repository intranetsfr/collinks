// Get Discord API object
const Discord = require('discord.js');

// Bot authentication token
const auth = require('./auth.json');

// Regexp to match links
const linkRegex = /(https?:\/\/[^ ,\)"\n`']*)/ig;

console.log(`Creating discord client`);

// Create a discord client object for the bot and run it
const bot = new Discord.Client();

// Discord ID of the bot (will be retreived from client when connected)
let botId;


function checkCreateChannel(guild, channelName) {
    ///ToDo: optimize for perf : cache destination channels (for each guild the bot is member of)
    //console.log('guild.channels.cache');
    //console.log(guild.channels.cache);
    //console.log('channel.guild.channels.cache');
    //console.log(channel.guild.channels.cache);
    let channel = guild.channels.cache.find(channel => channel.name === channelName);
    if (!channel) {
        console.log('Destination channel does not exists');
        ///ToDo: create channel
    } else {
        // All good
    }
    return channel;
}


// Handler called when discord client is connected
bot.on('ready', () => {
    console.log(`Logged in as ${bot.user.tag}!`);
    botId = bot.user.id;
    ///ToDo: Set bot status with bot.user.setActivity ?
});


// Handler called when an error occurs
bot.on('error', error => {
    console.log(`Error event received : ${error.message}`);
});


// Handler called when a message is posted
bot.on('message', msg => {
    console.log(`Message received from ${msg.author.id}/${msg.author.username} : ${msg.content}`);
    // Is the message a bot command?
    // A command message will start with `!`
    if (msg.content.substring(0, 1) == '!') {
        // Split message into command and arguments
        var args = msg.content.substring(1).split(' ');
        var cmd = args[0];       
        args = args.splice(1);

        console.log(`Processing command : ${cmd}`);

        // bot command switch
        switch(cmd) {
            case 'ping':
                // !ping command
                //msg.reply(':ping_pong: pong!');
                msg.channel.send(':ping_pong: pong!')
                break;
            // Add other command cases here ...
            default:
                // Unknown command
                console.log('WTF am I supposed to do?');
                msg.reply('No hablo español!');
        }
    } else {
        // Handle regular message
        if (msg.author.id === botId) { 
            console.log(`Ignoring message from self`);
        } else if (msg.author.bot) {
            console.log(`Ignoring message from another bot`);
        } else {
            const links = msg.content.match(linkRegex);
            if (links) {
                console.log(`found ${links.length} links in message content`);
                if (msg.channel.type === 'text') {
                    let dst = checkCreateChannel(msg.guild, 'liens-archivés');
                    if (dst) {
                        ///ToDo: catch exceptions (like this one : UnhandledPromiseRejectionWarning: DiscordAPIError: Missing Permissions)
                        dst.send(msg.content);
                        console.log('Message archived!');
                    } else {
                        //console.log(msg);
                        console.log('Destination channel not found!');
                    }
                } else {
                    console.log(msg);
                    console.log(`Messages from channel of type ${msg.type} are not supported (yet)`);
                }
            } else {
                //console.log(msg);
                console.log('No link found in message content');
            }

            ///ToDo: lookup stuff in msg.embeds: [] ?
            ///ToDo: lookup files in msg.attachments : Collection [Map]{string => MessageAttachment} ?
        }
    }
});


bot.login(auth.token);
