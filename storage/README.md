> [<- back](/README.md)

Collinks - storage
===================

## Intro

MongoDB engine has been choose to store the data.

### First, few lines about designning a JSON schema:

#### Golden rule

At the end, the document must fit the functional needs, entirely.

##### Golden rule explanation:

The goal is to have all information you need in only one document.
If you have to search multiple document for the basic usage of you application it just means you design is not the good one.
Here, the goal is basically to store URLs, so a document will be an URL and will contains all the information related to this URL.

Maybe we will create document collection for store tools information or configuration. Those informations wil be denormalize inside all URL documents.

### It's a trap #AdmiralAckbar:

#### Data type

In MongoDB: { "value":0 } != {"value":"0"} . There is no chance a string is equal to an int... #HighLanguage

[Use Null carefully](https://docs.mongodb.com/manual/tutorial/query-for-null-fields/)

Don't store raw data in dictionnary:

Not good : {"speed":[ 5,10,15]}

good : {"speed":[ {"value":5},{"value":10},{"value":15}]}

Why, because tomorrow when you will add a properties for example including the label (in order to show it instead of the value), it will be easier:

##### "Good case"

First you modify the database (all document or only the new one), from :

~~~json
{"speed":[ {"value":5},{"value":10},{"value":15}]}
~~~

to:

~~~json
{"speed":[ {"value":5,"label":"slow"},{"value":10,"label":"normal"},{"value":15,"label":"fast"}]}
~~~

And after you modify the application to use "speed.label". Your existing code will not been impacted by the database modification so you can do it when you want, even have different version of application code in parallel.

##### "Not Good" case

you have to request a big downtime to update at the same time the database and the application code with all associate risks

## Data Design

### Write Data flow

~~~mermaid
graph TB
  subgraph Collect
    WEBUI[Web UI] --> API1[Collint API]
    WEBext1[Browser Extension] --> API1
    Scripts[Scripts] --> A
      A[connect to the live source] -->API1
      API1 --> |Parse source| B(Collect URL and metadata comming from the source)
    end
    subgraph Storage
      B --> |API Call| Storage1[URL already exists ?]
      Storage1 -->|yes| Storage2[Add source in the document origin list]
      Storage1 -->|no| Storage3[Create New doc]
      Storage4[Update doc]
    end
    subgraph Scheduler
      Scheduler1[Pending Queue]
      Scheduler1 --> |Trigger|scheduler2[Module launcher]
    end
      Storage3 --> |Register| Scheduler1
    subgraph Crawler
      scheduler2 --> |Register| Crawler1[Syntax analisys]
      Crawler1 --> Storage4
    end
    Crawler1 -->|Re-Schedule| Scheduler1
~~~

### Read data flow

~~~mermaid
graph TB
  subgraph Collint API
    API1[search URL from TAG]
    API2[show URL document]
  end
  subgraph Storage
    API1 --> |Query| Storage1[URL Documents]
    API2 --> |Query|Storage1
  end
~~~

### Example for Data collecte
As a starting point, an example of schema (at the end this schema will be create in MongoDB validation schema).

~~~json
{
"url":"https://gitlab.com/crapaud-fou/collinks",
"global_info": 
  {
    "last_availability_date": Date("2019-09-25T16:00:00Z"),
    "last_analyzed_date": Date("2019-09-25T16:00:00Z"),
    "analyze_history": 
      [ 
        {"available":true, "date":Date("2019-09-25T16:00:00Z")},
        {"available":false, "date":Date("2019-09-24T16:00:00Z")},
        {"available":true, "date":Date("2019-09-24T16:00:00Z")},
      ],
    "raw_data":"<html>....</html>",
    "headers":
    [
      "name":"",
    ]
  }
"origin":
  [
    {
      "source":"RocketChat",
      "url_source":"https://coa.crapaud-fou.org/channel/collinks",
      "date_collect":Date("2019-09-25T16:00:00Z"),
      "properties":
          {"user":"Jamesse",
          "channel":"techos"},
    },
    {
      "source":"RocketChat",
      "url_source":"https://coa.crapaud-fou.org/channel/collinks",			
      "date_collect":Date("2019-09-22T16:00:00Z"),
      "properties":
          {"user":"mose",
          "channel":"techos"},

    },
    {
      "source":"Manual Web UI",
      "url_source":"https://coa.crapaud-fou.org/channel/collinks",
      "date_collect":Date("2019-09-21T16:00:00Z"),
      "properties":
          {"user":"Jamesse",
          "action":"Add URL"},
    },
  ],
"tags":
  [
    {"name":"collinks", "status":"deleted", "lock_status":true, "sources":
      [
        {"name":"Manual Web UI", "action":"delete", "date":Date("2019-09-22T16:00:00Z")},
        {"name":"crawler", "action":"add", "date":Date("2019-09-21T16:00:00Z")},
      ]
    },
    {"name":"collecteur", "status":"available", "sources":
      [
        {"name":"Manual Web UI", "action":"add", "date":Date("2019-09-21T16:00:00Z")},
      ]
    },
  ],
"bookmarks":
  [
    {
      "name":"Jamesse",
      "userid":1234,
      "options":{"type":"important"}
      "date":Date("2019-09-22T16:00:00Z"),
    },
    {
      "name":"Trenty",
      "userid":123,
      "date":Date("2019-09-22T16:00:00Z"),
    },
  ]
}
~~~

#### Example details

The document Key:
~~~json
"url":"https://gitlab.com/crapaud-fou/collinks",
~~~

All informations related to the URL and the target site:
~~~json
"global_info": 
  {
    "last_availability_date": Date("2019-09-25T16:00:00Z"),
    "last_analyzed_date": Date("2019-09-25T16:00:00Z"),
    "analyze_history":
      [
        {"availlable":true, "date":Date("2019-09-25T16:00:00Z")},
        {"availlable":false, "date":Date("2019-09-24T16:00:00Z")},
        {"availlable":true, "date":Date("2019-09-24T16:00:00Z")},
      ],
    "raw_data":"<html>....</html>",
    "headers":
    [
      "name":"",
    ]
  }
~~~

Informations about which tool collect the URL. It's a dictionary, because the URL can appears multiple time from the same source or multiple time in the same source.
It's not useful right now but for later analyze maybe.
Each property in the source is source dependent.

~~~json
"origin":
  [
    {
      "source":"RocketChat",
      "url_source":"https://coa.crapaud-fou.org/channel/collinks",
      "date_collect":Date("2019-09-25T16:00:00Z"),
      "properties":
          {"user":"Jamesse",
          "channel":"techos"},
    },
    {
      "source":"RocketChat",
      "url_source":"https://coa.crapaud-fou.org/channel/collinks",
      "date_collect":Date("2019-09-22T16:00:00Z"),
      "properties":
          {"user":"mose",
          "channel":"techos"},

    },
    {
      "source":"Manual Web UI",
      "url_source":"https://coa.crapaud-fou.org/channel/collinks",
      "date_collect":Date("2019-09-21T16:00:00Z"),
      "properties":
          {"user":"Jamesse",
          "action":"Add URL"},
    },
  ],
~~~

Tags will be filed by tools, so we have to track who have do which action:

~~~json
"tags":
  [
    {"name":"collinks", "status":"deleted", "lock_status":true, "sources":
      [ 
        {"name":"Manual Web UI", "action":"delete", "date":Date("2019-09-22T16:00:00Z")},
        {"name":"crawler", "action":"add", "date":Date("2019-09-21T16:00:00Z")},
      ]
    },
    {"name":"collecteur", "status":"available", "sources":
      [ 
        {"name":"Manual Web UI", "action":"add", "date":Date("2019-09-21T16:00:00Z")},
      ]
    },
  ]
~~~

In bookmarks we will store every user interested by the link an want to store it in his personal space.

~~~json
"bookmarks":
  [
    {
      "name":"Jamesse",
      "userid":1234,
      "options":{"type":"important"}
      "date":Date("2019-09-22T16:00:00Z"),
    },
    {
      "name":"Trenty",
      "userid":123,
      "date":Date("2019-09-22T16:00:00Z"),
    },
  ]
~~~

### Example for Data source configuration

An example of schema in order to store source list (at the end this schema will be create in MongoDB validation schema).

~~~json
{
    "type":"RocketChat",
    "name":"Crapaud",
    "collect_frequency":"Daily",
    "URL":"https://coa.crapaud-fou.org/",
    "username":"Jamesse",
    "password":password(FEZO79974),
    "creation":
    {
        "date":Date("2019-09-21T16:00:00Z"),
        "user":"Jamesse"
    },
    "last_5_collect":
    [
        {
            "date":Date("2019-09-21T16:00:00Z"),
            "Status":"OK",
        },
        {
            "date":Date("2019-09-20T16:00:00Z"),
            "Status":"KO",
            "Error":"Timeout url 10 Sec"
        },
    ],
    "exclude_username":
        [
            {
                "username":"crabot",
                "date":Date("2019-09-20T16:00:00Z"),
                "excluded_by":"Jamesse",
            },
        ]
}
~~~

### Example for admin user management

An example of schema in order to store source list (at the end this schema will be create in MongoDB validation schema).

~~~json
{
    "username":"Jamesse",
    "email":"jamessse@jamesse.com"
    "password":password(FEZO79974),
    "creation":
    {
        "date":Date("2019-09-21T16:00:00Z"),
    },
    "last_login":2019-09-21T16:00:00Z"),
    "grants":
        [
            {
                "type":"admin",
                "date":Date("2019-09-20T16:00:00Z"),
                "granted_by":"Jamesse",
            },
            {
                "type":"collect",
                "source_list":
                    [
                        {"type":"RocketChat","name":"Crapaud"},
                    ]
                "date":Date("2019-09-20T16:00:00Z"),
                "granted_by":"Jamesse",
            },
        ]
}
~~~
