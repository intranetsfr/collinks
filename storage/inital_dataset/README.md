> [<- back](/README.md)

Collinks - storage - inital_dataset
===================================

The goal is to create the first dataset in order to have a database base to be able to work.
The first load wil be created by parsing a rocketchat.

### environement variables

You have to set os variabe to launch the script:

ROCKETCHAT_USERNAME=xxxx@xxxxx.net
ROCKETCHAT_PASSWORD=xxxxx
ROCKETCHAT_SERVER=https://xxxxxxx.org

### MongoDB Integration

We wil create a collection for each rocketchat server.
In this colection we will have all the channel reated to the server and we will store the last parsing date on it.

### Process

Due to the rocketchat ratelimiter, we wil wait 90 sec each time we dump 10 channels.
Default ratelimiter configuration on rocketchat : 10 API CALL per 60 sec.

### Docker compose

First, set the environement variabe in your shell. See environement variables paragraph.
For the moment that only launch the dump of a channel to test the docker, nothing more than that.
On this directory, launch the two the docker with docker-compose:

~~~~bash
docker-compose up -d
~~~~

Verify the mongodb container is up and app container run successfully:

~~~~bash
 docker-compose ps
~~~~

Network and so DNS to use is: "app-tier"

