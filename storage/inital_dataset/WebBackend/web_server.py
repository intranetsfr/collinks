#! /usr/bin/python
# -*- coding:utf-8 -*-

from flask import Flask, session, flash, request, redirect, url_for
from flask import render_template
import os
import pymongo
from urllib.parse import quote_plus

app = Flask(__name__)

@app.context_processor
def passer_titre():
    #current_status=AutomateProcess.process_is_running()
    return dict(titre="collinks",current_status="No process")

@app.route('/')
def index():
    user="collinks"
    password="DontCareForTheMoment"
    host="database"
    database="collinks"
    uri = "mongodb://%s:%s@%s/%s" % (quote_plus(user), quote_plus(password), host,database)
    client = pymongo.MongoClient(uri)
    db = client.collinks
    test = db.test
    test.insert_one({"test":'ok'})
    return render_template('info.html', result=str(test.find_one()))



@app.route('/clearsession')
#@login_required
def clearsession():
    session.clear()
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')