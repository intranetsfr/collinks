import random

def getColor():
    r = random.randrange(255)
    g = random.randrange(255)
    b = random.randrange(255)
    return 'rgb({:0},{:0},{:0})'.format(r,g,b)

def createElement(label, color, data) :
  return {
    "label": label,
    "backgroundColor": color,
    "data": data
  }

def setTopicInfo(tsunamy, messagesDataTopic, id, length):
  if tsunamy & Topics.GLOBAL:
    messagesDataTopic[Topics.GLOBAL][id] += length
  if tsunamy & Topics.PROJECT:
    messagesDataTopic[Topics.PROJECT][id] += length
  if tsunamy & Topics.DEMOCRACY:
    messagesDataTopic[Topics.DEMOCRACY][id] += length
  if tsunamy & Topics.ECOLOGY:
    messagesDataTopic[Topics.ECOLOGY][id] += length
  if tsunamy & Topics.TECHNOLOGY:
    messagesDataTopic[Topics.TECHNOLOGY][id] += length

def getTopic(channel):
    value = Topics.GLOBAL
    if 'description' in channel:
        if channel['description'].find("#projet") != -1:
            value |= Topics.PROJECT
        if channel['description'].find("#democratie") != -1:
            value |= Topics.DEMOCRACY
        if channel['description'].find("#ecologie") != -1:
            value |= Topics.ECOLOGY
        if channel['description'].find("#technologie") != -1:
            value |= Topics.TECHNOLOGY
    return value

class Topics:
    GLOBAL      = 1 << 0
    PROJECT     = 1 << 1
    DEMOCRACY   = 1 << 2
    ECOLOGY     = 1 << 3
    TECHNOLOGY  = 1 << 4