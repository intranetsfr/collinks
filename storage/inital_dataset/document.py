from datetime import datetime
import re, logging

class Origin:
    def __init__(self, source, url, properties):
        checkURL(url)
        self.source = source
        self.url_source = url
        self.date_collect = datetime.now()
        self.properties = properties

    def toJSON(self):
        json = {
            "source": self.source,
            "url_source": self.url_source,
            "date_collect": self.date_collect.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            "properties": self.properties
        }
        return json
    
class Global_Info:
    def __init__(self):
        self.last_availability_date = datetime.now()
        self.last_analyzed_date = datetime.now()
        self.analyze_history = []
        self.raw_data = ""
        self.headers = []       

    def toJSON(self):
        json = {
            "last_availability_date": self.last_availability_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            "last_analyzed_date": self.last_analyzed_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            "analyze_history": self.analyze_history,
            "raw_data": self.raw_data,
            "headers": self.headers
        }
        return json

class Document:
    def __init__(self, url):
        checkURL(url)
        self.url = url
        self.global_info = Global_Info()
        self.origin = []
        # self.tags = []
        # self.bookmarks = []

    def toJSON(self):
        json = {
            "url": self.url,
            "global_info": self.global_info.toJSON(),
            "origin": [origin.toJSON() for origin in self.origin]
        }
        
        return json

    def addOrigin(self, origin: Origin):
        self.origin.append(origin)

def checkURL(url):
    if len(re.findall("https?://.+", url)) > 0:
        logging.warning("Url ({}) haven't good format".format(url))
        return True
    return False
